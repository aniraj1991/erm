/*
Var Project = {
	init = function()
	{
		console.log("Init function ");
	}
};

Var Project = function(){
	this.limit = 10;
};

Project.init = function()
{
	console.log("Init function ");
	console.log("Limit value -  " + this.limit);
};


function Project(){};
Project.prototype.init = function()
{
	console.log("Init function ");
};

Project projectObj = new Project();


Project.init();
projectObj.init(); 

*/

var Project = {
	doRenderEmployeList : function(name)
	{
		var isNamePresent = false; 
		if($(".empName").length)
		{
			$(".empName").each(function(){
				var empName = $(this).text();
				if(empName && empName == name)
				{
					isNamePresent = true;
					return;
				}
			});
		}
		if(isNamePresent)
		{
			$(".empError").show();
			setTimeout(function(){
				$(".empError").hide(500);
			}, 3000);
		}
		else
		{
			if(name)
			{
				var employeeHtml = "<div class='ermEmpInner'> <div class='empName'>" + name + "</div> <div class='empCloseButton'> X </div></div>";
				$(".employeeList").append(employeeHtml);
			}
			$("#managername").val(name);
			this.renderEmployeeEvent();
		}
	},
	renderEmployeeEvent : function()
	{
		$(".empCloseButton").unbind().bind('click', function(){
			var parentEle = $(this).parent();
			if(parentEle)
			{
				$(parentEle).remove();
			}
			Project.showEmployeeList();
		});
		this.showEmployeeList();
	},
	showEmployeeList : function()
	{
		if($(".ermEmpInner").length)
		{
			$(".employeeList").show();
		}
		else
		{
			$(".employeeList").hide();
		}
	}
};


    $(document).ready(function(){
      var date_input=$('input[name="startDate"],input[name="endDate"]'); 
      var container= $('form').parent();
      var options={
        format: 'dd/mm/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);
      
      $(".suggestion").keyup(function(evt){
    	  var val = evt.currentTarget.value;    	  
    	  if(val.length > 2){    		  
    		  var showNameCbk = function(data){
    			  if(data ){
    				  var dataArray = JSON.parse(data);   
    				  
    				  var dropDown = "<div class='searchSuggestion'>";
    				  for(var i=0; i < dataArray.length; i++)
    				  {
				
    					  dropDown = dropDown +  "<div class='selectedResources'>" + dataArray[i].name + "</div>";		
    				  }
    				  
    				  dropDown = dropDown + "</div>";
    				  
    				  
    				  $(".searchSuggestion").html(dropDown).show();
    				  $(".selectedResources").click(function(event){ 
    					  //$("#managername").val(event.currentTarget.innerText);
    					  var selectedName = $(this).text();
    					  if(selectedName)
    					  {
    						  Project.doRenderEmployeList(selectedName)
    						  $(".searchSuggestion").html("").hide();
    						  $(".suggestion").val("");
    					  }   					  
    				  });
    			  }   			 
    		  }
    		  var url = "http://localhost:8102/employee/searchterm?searchterm=" + val;
    		  $.ajax({
    			  url: url,
    			  success: showNameCbk,
    			  error: showNameCbk
    			  });
    	  }
    	  
      });
    })
