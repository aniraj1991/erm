function checkAuthSession(){
	var cookieVal = document.cookie.split(";");
	
	if(!(cookieVal && cookieVal.length > 1 && cookieVal.indexOf(" isLoggedIn=true") != -1)){
		window.location.href = "http://localhost:8102/user/login";
	}
}

function logoutSession(){
	
	var delete_cookie = function(nameArr) {
		
		nameArr.forEach(function(name){
		    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		});
		
		window.location.href = "http://localhost:8102/user/logout";
	};
	delete_cookie(["isLoggedIn","user-id","user-privilege"]);
}
function navToLogInPage(){
	window.location.href = "http://localhost:8102/user/login";
}