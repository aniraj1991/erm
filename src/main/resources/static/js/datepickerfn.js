$(document).ready(function(){
	
     var date_input=$('input[name="doj"],input[name="dob"]');
     var startDate_input=$('input[name="startDate"]');
     var container= $('form').parent();
     var options={
    		 dateFormat: "dd/mm/yy",
       container: container,
       todayHighlight: true,
       autoclose: true
     };
     var options1={
    		 dateFormat: "dd/mm/yy",
    		 setDate: "today"
     };
     date_input.datepicker(options);
     startDate_input.datepicker(options1);
     
    
     $('.cb-value').click(function() {
		  var mainParent = $(this).parent('.toggle-btn');
		  if($(mainParent).find('input.cb-value').is(':checked')) {
		    $(mainParent).addClass('active');
		    $(".cls_userRoleCont").show();
		  } else {
		    $(mainParent).removeClass('active');
		    $(".cls_userRoleCont").hide();
		  }

		})
});