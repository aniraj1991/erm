$(document).ready(function(){
     var  getCookie = function(cname){
    	  var name = cname + "=";
    	  var decodedCookie = decodeURIComponent(document.cookie);
    	  var ca = decodedCookie.split(';');
    	  for(var i = 0; i <ca.length; i++) {
    	    var c = ca[i];
    	    while (c.charAt(0) == ' ') {
    	      c = c.substring(1);
    	    }
    	    if (c.indexOf(name) == 0) {
    	      return c.substring(name.length, c.length);
    	    }
    	  }
    	  return "";
    	}
     
     $("#loadMore").unbind("click").bind("click",function(){
    	 var offset= $("#content tr").length;
    	 var limit = 10;
    	 var user_privilege = getCookie("user-privilege");
    	 var emailid = getCookie("user-id");
    	
    	 var successFn = function(data)
    	 {
    		 console.log(data);
    		 var source   = document.getElementById("entry-template").innerHTML;
        	 var template = Handlebars.compile(source);
      
        	 if(data && data.employeeList && data.employeeList.length > 0)
        	 {
        		 var html    = template(data);
            	 $("#content").append(html)
        	 }
        	 else
        	 {
        		 $("#loadMore").hide();
        	 }
        	 
    	 }
    	 $.ajax({"url":"/employee/viewbyall?offset="+offset+"&limit="+limit +"&user_privilege="+user_privilege+"&emailid="+emailid,
    		 success: function(result)
    		 {
    			 successFn(result);
    		 },
    		 error: function(result)
    		 {
    		    successFn(result);   
    		 },
    		 
    		 xhrFields: {
    			    // The 'xhrFields' property sets additional fields on the XMLHttpRequest.
    			    // This can be used to set the 'withCredentials' property.
    			    // Set the value to 'true' if you'd like to pass cookies to the server.
    			    // If this is enabled, your server must respond with the header
    			    // 'Access-Control-Allow-Credentials: true'.
    			    withCredentials: false
    			  },
    		/* crossDomain: true,
    		 dataType: "json"*/
    	 })
    	 
     })
});