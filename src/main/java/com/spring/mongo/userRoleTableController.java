/*******************************************************************************
 * Copyright ©2002-2014 Skava.
 * All rights reserved.The Skava system, including
 * without limitation, all software and other elements
 * thereof, are owned or controlled exclusively by
 * Skava and protected by copyright, patent, and
 * other laws. Use without permission is prohibited.
 *
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.spring.mongo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class userRoleTableController {
	
	@Autowired
	public userRolesReposetory userroles;
	
	@GetMapping("/Users/Role/GetAll")
	public List<userRoles> GetUserRoles()
	{
		return (List<userRoles>) userroles.findAll();
	}
	
	@GetMapping("/Users/Role/GetById/{id}")
    public Optional<userRoles> GetRoles(@PathVariable int id)
    {
    	return userroles.findByUserid(id);	
    }
    
    @GetMapping("/Users/Role/GetByPrivilegeId/{priId}")
    public Optional<userRoles> GetPrivilege(@PathVariable int priId)
    {
    	return userroles.findByPrivilegeid(priId);	
    }
    
    @PostMapping(value="/Users/AddRoles", consumes = {"application/json"})
	public userRoles createRoles(@RequestBody userRoles r)
	{
		System.out.println(r);
		userroles.save(r);
		return r;
	}
}
