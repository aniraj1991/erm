package com.spring.mongo;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
//import org.springframework.web.servlet.ModelAndView;

@Component
public class AdminView {
	// @Autowired AdminServiceImpl adminServiceImpl;
	// RestTemplate restTemplate = new RestTemplate();

	public project addproject() {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8082/productadd";
		project result = restTemplate.getForObject(url, project.class);
		return result;
	}

	public users addnewuser(users usersdata) {
		RestTemplate restTemplate = new RestTemplate();
		// JSONObject js=new JSONObject();
		String url = "http://localhost:8102/Users/add";
		HttpHeaders headers = new HttpHeaders();
		// headers.set("Content-Type", "application/json");

		// HttpEntity<JSONObject> request = new HttpEntity<JSONObject>(usersdata,
		// headers);
		users result = restTemplate.postForObject(url, usersdata, users.class);
		return result;
	}

	public userRoles addNewUserRole(userRoles userRole) {
		userRoles result = null;
		if (userRole != null && userRole.getPrivilegeid() > 0 && userRole.getUserid() > 0) {
			RestTemplate restTemplate = new RestTemplate();
			// JSONObject js=new JSONObject();
			String url = "http://localhost:8102/Users/AddRoles";
			HttpHeaders headers = new HttpHeaders();
			// headers.set("Content-Type", "application/json");

			// HttpEntity<JSONObject> request = new HttpEntity<JSONObject>(usersdata,
			// headers);
			result = restTemplate.postForObject(url, userRole, userRoles.class);
		} else {
			System.out.println("Unable to process the request");
		}
		return result;
	}

	public List<project> assetContent(String url) {
		RestTemplate restTemplate = new RestTemplate();
		List<project> result = restTemplate.getForObject(url, List.class);
		return result;
	}

	public List<project> searchpageview(String search) {
		RestTemplate restTemplate = new RestTemplate();
		List<project> result = restTemplate.getForObject("http://localhost:8082/search?search=" + search, List.class);
		return result;
	}

	public project assetContentbid(String id) {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8082/viewbyid?id=" + id;
		project result = restTemplate.getForObject(url, project.class);
		return result;
	}

	public project assetContentbyname(String name) {
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8082/project/getbyname/" + name;
		project result = restTemplate.getForObject(url, project.class);
		return result;
	}

	public project submitdata(project project) {
		// TODO Auto-generated method stub
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8082/productadd";
		project result = restTemplate.postForObject(url, project, project.class);
		return result;
	}

	public Employee submitemployeedata(Employee employee, String id) {
		// TODO Auto-generated method stub
		RestTemplate restTemplate = new RestTemplate();
		String url = "http://localhost:8102/employee/update/" + id;
		Employee result = restTemplate.postForObject(url, employee, Employee.class);
		return result;
	}
}
