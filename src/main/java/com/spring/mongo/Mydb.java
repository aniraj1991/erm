package com.spring.mongo;

import java.util.List;

//import java.awt.List;

import org.json.JSONArray;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sun.xml.internal.fastinfoset.util.StringArray;

import lombok.Getter;
import lombok.Setter;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL )
@Document
public class Mydb {
	//@Id
	 @Getter @Setter private String id;
	 @Getter @Setter private String name;
	 @Getter @Setter private String emailId;
	@Getter @Setter private String employeeId;
	@Getter @Setter private String doj;
	@Getter @Setter private String address;
	@Getter @Setter private String contact;
	@Getter @Setter private String dob;
	@Getter @Setter private String designation;
	@Getter @Setter private String experience;
	@Getter @Setter private 	List<String> skillSet;
	@Getter @Setter private List<project> project;
	@Getter @Setter private List<managerDetails> managerDetails;
//	@Getter @Setter private JSONArray project;

}
