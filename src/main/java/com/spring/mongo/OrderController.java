package com.spring.mongo;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.catalina.User;
import org.json.JSONArray;
import org.json.JSONObject;
//import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

//import com.Service.EmployeeModel.Employee;
//import com.Service.EmployeeRepository.EmployeeRepository;
//import com.Service.EmployeeRepository.EmployeeSearchRepository;

@RestController
public class OrderController {
	private final Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	public OrderRepository orderRepo;

	@Autowired
	public AdminView adminviewob;

	@Autowired
	EmployeeRepository employeerepository;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	EmployeeSearchRepository employeeSearchRepository;

	public OrderController(OrderRepository project) {
		this.orderRepo = project;
	}

	/*
	 * @GetMapping("/orderList") public List getAllOrders() {
	 * log.info("in get all users"); return orderRepo.findAll(); }
	 */

	/*
	 * @PostMapping("/employee/add") public Mydb employee(@RequestBody Mydb order) {
	 * log.info("in create users"); return orderRepo.save(order); }
	 */

	public Boolean isLoggedin(String user_privilege) {
		if (user_privilege == "1" || user_privilege == "2" || user_privilege == "3") {
			return true;
		}
		return false;
	}

	@PostMapping("/project/add")
	public project project(@RequestBody project order) {
		log.info("in create project");
		return orderRepo.save(order);
	}

	@GetMapping("/project/viewall")
	public List<project> getAllOrders(@RequestParam(value = "offset", defaultValue = "0") int offset,
			@RequestParam(value = "limit", defaultValue = "10") int limit,
			@RequestParam(value = "user_privilege", defaultValue = "3") String user_privilege,
			@RequestParam(value = "emailid") String emailid) {
		log.info("in get all users");
		if (user_privilege.equals("3")) {
			Employee employee = employeeRepository.findByEmailId(emailid);
			String projectname = employee.getProjectDetails();
			return orderRepo.findByName(projectname);

			// return orderRepo.findByTypeofproject("id);
		}

		return employeeSearchRepository.getAllProject(offset, limit);
	}

	@GetMapping("/project/getbyid/{id}")
	public Optional<project> employee_getbyid(@PathVariable("id") String id) {

		return orderRepo.findById(id);

	}

	@GetMapping("/project/getbyvalue")
	public List<project> employee_getbyname(@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "description", required = true) String description,
			@RequestParam(value = "client", required = true) String client) {
		return orderRepo.findByNameRegexOrDescriptionRegexOrClientRegex(name, description, client);

	}

	@RequestMapping(value = "/viewpage", method = RequestMethod.GET)
	public ModelAndView viewpage(@ModelAttribute User user) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("asset-data");
		modelAndView.addObject("asset", adminviewob.assetContent());
		return modelAndView;
	}

	@RequestMapping(value = "/searchpageview", method = RequestMethod.GET)
	public ModelAndView searchpageview(@RequestParam(value = "search", required = false) String search) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("asset-data");
		modelAndView.addObject("asset", adminviewob.searchpageview(search));
		return modelAndView;
	}

	@RequestMapping(value = "/viewid", method = RequestMethod.GET)
	public ModelAndView viewpageid(@RequestParam(value = "id", required = false) String id) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("projectviewpage");
		modelAndView.addObject("asset", adminviewob.assetContentbid(id));
		return modelAndView;
	}

	@RequestMapping(value = "/searchbyname", method = RequestMethod.GET)
	public ModelAndView searchbyname(@RequestParam(value = "name", required = false) String name) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("assetbyid");
		modelAndView.addObject("asset", adminviewob.assetContentbid(name));
		return modelAndView;
	}

	@RequestMapping(value = "/submitdata", method = RequestMethod.POST)
	public ModelAndView buttonview(@ModelAttribute project project) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("asset-data");
		modelAndView.addObject("asset", adminviewob.submitdata(project));
		return modelAndView;
	}

	@RequestMapping(value = "/addprojectview", method = RequestMethod.GET)
	public ModelAndView addprojectview(@RequestParam(value = "id", required = false) String id, Model model) {
		ModelAndView modelAndView = new ModelAndView();
		if (!(id == null)) {
			modelAndView.setViewName("projecteditview");
			modelAndView.addObject("asset", adminviewob.assetContentbid(id));
		} else
			modelAndView.setViewName("addprojectview");

		return modelAndView;
	}
	// EMPLOYEE

	/*
	 * @RequestMapping(value = "/employee/add", method = RequestMethod.POST)
	 * public @ResponseBody String addEmployee(@RequestBody Employee employee) {
	 * 
	 * employeeRepository.save(employee); return "Saved Employee deatils"; }
	 */

	@RequestMapping(value = "/employee/search/{search}", method = RequestMethod.GET)
	public Collection<Employee> searchEmployee(@PathVariable String search) {

		return employeeSearchRepository.getEmployees(search);
	}

	@RequestMapping(value = "/employee/searchterm", method = RequestMethod.GET)
	public ResponseEntity<String> searchEmployeereturnvalue(@RequestParam(value = "searchterm") String searchterm) {

		Collection<Employee> employee = employeeSearchRepository.getEmployees(searchterm);
		Iterator<Employee> iterator = employee.iterator();
		JSONArray namearry = new JSONArray();
		while (iterator.hasNext()) {
			Employee employyeadd = iterator.next();
			JSONObject js = new JSONObject();
			if (employyeadd != null && employyeadd.getLevel() == 1) {
				if (employyeadd.getName() != null && employyeadd.getName().length() > 0) {
					js.put("name", employyeadd.getName());
				}
				if (employyeadd.getEmailId() != null && employyeadd.getEmailId().length() > 0) {
					js.put("emailId", employyeadd.getEmailId());
				}
				if (employyeadd.getId() != null && employyeadd.getId().length() > 0) {
					js.put("employeeId", employyeadd.getId());

				}
				if (js.length() > 0) {
					namearry.put(js);
				}
			}
		}
		return new ResponseEntity<String>(namearry.toString(), HttpStatus.OK);
	}

	@RequestMapping(value = "/employee/viewbyid/{empId}", method = RequestMethod.GET)
	public Optional<Employee> getEmployeebyId(@PathVariable String empId) {

		return employeerepository.findById(empId);
	}

	/*
	 * @RequestMapping(value = "/employee/delete/{empId}", method =
	 * RequestMethod.GET) public String deleteEmployee(@PathVariable String empId) {
	 * if (employeeSearchRepository.getEmployeesById(empId) != null) {
	 * employeeRepository.delete(empId); return "Deleted"; } else { return
	 * "employee id is not available"; }
	 * 
	 * }
	 */

	/*
	 * @RequestMapping(value = "/employee/update/{empId}", method =
	 * RequestMethod.POST) public @ResponseBody String updateEmployee(@RequestBody
	 * Employee employee) { if
	 * (employeeSearchRepository.getEmployeesById(employee.getEmployeeId()) != null)
	 * { employeeRepository.save(employee); return "updated Employee deatils"; }
	 * else { return "Not available Employee deatils"; }
	 * 
	 * }
	 */

	@RequestMapping(value = "/employee/update/{empId}", method = RequestMethod.POST)
	public @ResponseBody Employee updateEmployee(@RequestBody Employee employee, @PathVariable String empId) {
		System.out.println("update call...");
		Optional<Employee> prevEmpObj = employeerepository.findById(empId);
		// employee.setId(empId);

		/*
		 * if(prevEmpObj != null ) { if(prevEmpObj.getPid() != null &&
		 * prevEmpObj.getPid() != employee.getPid()) { List<projectpid>
		 * previousProjectDetails = prevEmpObj.getProjectDetails(); projectpid project =
		 * new projectpid(); project.setPid(prevEmpObj.getPid());
		 * project.setPname(prevEmpObj.getPname());
		 * project.setStartDate(prevEmpObj.getStartDate());
		 * project.setEndDate(prevEmpObj.getStartDate());
		 * 
		 * if(previousProjectDetails == null) { previousProjectDetails = new
		 * ArrayList<projectpid>(); } previousProjectDetails.add(project);
		 * employee.setProjectDetails(previousProjectDetails); }
		 */
		return employeeRepository.save(employee);
		// return "updated Employee deatils";
	}
	/*
	 * else { return "Not available Employee deatils"; }
	 * 
	 * }
	 */

	// @RequestMapping(value = "Employee", method = RequestMethod.POST)
	@GetMapping("/employee/employeeview")
	public ModelAndView getAllEmployee(@CookieValue(value = "user-privilege", defaultValue = "3") String user_privilege,
			@CookieValue(value = "user-id") String emailid) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("view");
		modelAndView.addObject("userprivilege", user_privilege);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new TestErrorHandler());
		String url = null;

		url = "http://localhost:8102/employee/viewbyall?offset=0&limit=10&user_privilege=" + user_privilege + "&emailid="
				+ emailid;

		org.json.simple.JSONObject result = restTemplate.getForObject(url, org.json.simple.JSONObject.class);
		modelAndView.addObject("employeeList", result.get("employeeList"));
		return modelAndView;
	}

	public String addEmployeeCall(@ModelAttribute Employee product) {
		System.out.println("create products");
		HttpHeaders headers = new HttpHeaders();
		// headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<Employee> entity = new HttpEntity<Employee>(product, headers);

		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.exchange("http://localhost:8082/employee/create", HttpMethod.POST, entity, String.class)
				.getBody();
	}

	@RequestMapping(value = "/employee/addEmployee", method = RequestMethod.POST)
	public ModelAndView addEmployee(@ModelAttribute @Valid Employee employee, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("view");
		String result = this.addEmployeeCall(employee);
		if (result != null) {

			users users1 = new users();
			if (employee.getEmailId() != null) {
				users1.setUseremail(employee.getEmailId());
				// users.put("useremail", employee.getEmailId());
			}
			if (employee.getId() != null) {
				users1.setUserempid(employee.getId());
				// users.put("userempid", employee.getId());
			}
			if (employee.getName() != null) {
				users1.setUsername(employee.getName());
				// users.put("username", employee.getName());
			}
			users1.setUserpwd("password");
			// users.put("userpwd", "12345");
			int value = 2;
			// score = 0;
			users1.setUserstatus(value);
			// modelAndView.addObject("asset", adminviewob.addnewuser(users1));
			users1 = adminviewob.addnewuser(users1);
			if (users1 != null && users1.getId() > 0) {

				int userId = (int) users1.getId();
				int privilegeId = 2;
				if (employee.getUserRole() != null && !employee.getUserRole().trim().isEmpty()) {
					privilegeId = Integer.valueOf(employee.getUserRole());
				}

				userRoles userRole = new userRoles();
				userRole.setUserid(userId);
				userRole.setPrivilegeid(privilegeId);
				
				userRole = adminviewob.addNewUserRole(userRole);
			}
		}
		System.out.print(result);

		modelAndView.addObject("result", result);
		modelAndView.addObject("employee", employee);
		modelAndView.addObject("employeeList", employeeRepository.findAll());
		// }
		return new ModelAndView("redirect:" + "http://localhost:8102/employee/employeeview");

	}

	@RequestMapping(value = "/employee/insert", method = RequestMethod.POST)
	public @ResponseBody String addEmployee(@RequestBody Employee employee) {

		employeeRepository.save(employee);
		return "Saved Employee deatils";
	}

	@RequestMapping(value = "/employee/projectview", method = RequestMethod.GET)
	public ModelAndView viwAllProject(@CookieValue(value = "user-privilege", defaultValue = "3") String user_privilege,
			@CookieValue(value = "user-id") String emailid) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("asset-data");
		String url = null;
		url = "http://localhost:8102/project/viewall?offset=0&limit=10&user_privilege=" + user_privilege + "&emailid="
				+ emailid;

		/*
		 * org.json.simple.JSONObject result = restTemplate.getForObject(url,
		 * org.json.simple.JSONObject.class); modelAndView.addObject("employeeList",
		 * result.get("employeeList")); return modelAndView;
		 */
		modelAndView.addObject("asset", adminviewob.assetContent(url));
		modelAndView.addObject("userprivilege", user_privilege);

		return modelAndView;
	}

	@RequestMapping(value = "/employee/searchEmployee", method = RequestMethod.GET)
	public ModelAndView searchEmployeeadd(@ModelAttribute Employee employee,
			@RequestParam(value = "search", required = true) String search) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("view");
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new TestErrorHandler());
		String url = "http://localhost:8082/employee/searchEmployee/" + search;
		List<Employee> result = restTemplate.getForObject(url, List.class);
		// System.out.print(result);
		// modelAndView.addObject("result", result);
		modelAndView.addObject("search", search);
		if (result.isEmpty()) {
			modelAndView.addObject("result", "No Employee details are found for " + search);
		} else {
			modelAndView.addObject("result", "Employee details for \"" + search + "\": ");
		}
		modelAndView.addObject("employeeList", result);
		System.out.println("result:" + result.toString());

		return modelAndView;
	}

	@RequestMapping(value = "/employee/viewEmployee", method = RequestMethod.GET)
	public ModelAndView viewEmployeebyId(@RequestParam(value = "id", required = true) String empid) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("profile");
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new TestErrorHandler());
		String url = "http://localhost:8082/employee/viewEmployeebyid/" + empid;
		Employee employee = restTemplate.getForObject(url, Employee.class);
		// System.out.print(result);
		// modelAndView.addObject("result", result);
		modelAndView.addObject("search", empid);
		if (employee == null) {
			modelAndView.addObject("result", "No Employee details are found for " + empid);
		} else {
			modelAndView.addObject("result", "Employee details for \"" + empid + "\": ");
		}
		modelAndView.addObject("employeeList", employee);
		// System.out.println("result:" + employee.toString());

		return modelAndView;
	}

	@RequestMapping(value = "/employee/add", method = RequestMethod.GET)
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView();
		Employee employee = new Employee();
		modelAndView.addObject("employee", employee);
		modelAndView.setViewName("addemployee");

		// System.out.print(result);
		// modelAndView.addObject("result", result);
		// modelAndView.addObject("employeeList", employeeRepository.findAll());
		return modelAndView;
	}
	/*
	 * @RequestMapping(path = "/add") public String add() {
	 * //carRepository.save(car); return "addemployee"; }
	 */

	@RequestMapping(path = "/employee/search")
	public String search(Model model, @RequestParam String search) {
		model.addAttribute("employeeList", employeeSearchRepository.getEmployees(search));
		model.addAttribute("search", search);
		return "home";
	}

	@RequestMapping(path = "/employee/searchMaangerDetails")
	public String getAllMaangerDetails(Model model, @RequestParam String searchTxt) {
		model.addAttribute("employeeList", employeeSearchRepository.getManagerDetails(searchTxt));
		model.addAttribute("search", searchTxt);
		return "home";
	}

	@RequestMapping(value = "/employee/editEmployee", method = RequestMethod.GET)
	public ModelAndView editEmployeebyId(@RequestParam(value = "id", required = true) String empid) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("update_employee");
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new TestErrorHandler());
		String url = "http://localhost:8082/employee/viewEmployeebyid/" + empid;
		Employee employee = restTemplate.getForObject(url, Employee.class);
		modelAndView.addObject("search", empid);
		if (employee == null) {
			modelAndView.addObject("result", "No Employee details are found for " + empid);
		} else {
			modelAndView.addObject("result", "Employee details for \"" + empid + "\": ");
			modelAndView.addObject("employee", employee);
		}
		return modelAndView;
	}

	public String updateEmployeeCall(String id, @ModelAttribute Employee product) {
		System.out.println("create products");
		HttpHeaders headers = new HttpHeaders();
		// headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<Employee> entity = new HttpEntity<Employee>(product, headers);

		RestTemplate restTemplate = new RestTemplate();
		return restTemplate
				.exchange("http://localhost:8082/employee/updateEmployee/" + id, HttpMethod.POST, entity, String.class)
				.getBody();
	}

	@RequestMapping(value = "/employee/updateEmployee", method = RequestMethod.POST)
	public ModelAndView updateEmployeedata(@ModelAttribute @Valid Employee employee, BindingResult bindingResult,
			@RequestParam(value = "id", required = true) String id) {
		ModelAndView modelAndView = new ModelAndView();
		// employee.id = id;
		if (bindingResult.hasErrors()) {

			modelAndView.setViewName("update_employee");
			// modelAndView.addObject("result", "This model have some error.");
			// modelAndView.addObject("employeeList", employeeRepository.findAll());
		} else {
			// ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("view");
			modelAndView.addObject("employeeList", adminviewob.submitemployeedata(employee, id));
			return modelAndView;

			/*
			 * modelAndView.setViewName("view"); String result = this.updateEmployeeCall(id,
			 * employee); modelAndView.addObject("result", result);
			 * modelAndView.addObject("employeeList", employeeRepository.findAll());
			 */
		}
		return modelAndView;
	}

	@RequestMapping(value = "/employee/updateEmploy/{employeeId}", method = RequestMethod.POST)
	public ModelAndView updateEmployee1(@ModelAttribute @Valid Employee employee, BindingResult bindingResult,
			@PathVariable String employeeId) {
		ModelAndView modelAndView = new ModelAndView();
		// employee.id = employeeId;
		if (bindingResult.hasErrors()) {

			modelAndView.setViewName("update_employee");
			// modelAndView.addObject("result", "This model have some error.");
			// modelAndView.addObject("employeeList", employeeRepository.findAll());
		} else {
			// ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("viewid");
			modelAndView.addObject("employeeList", adminviewob.submitemployeedata(employee, employeeId));
			return modelAndView;

			/*
			 * modelAndView.setViewName("view"); String result = this.updateEmployeeCall(id,
			 * employee); modelAndView.addObject("result", result);
			 * modelAndView.addObject("employeeList", employeeRepository.findAll());
			 */
		}
		return modelAndView;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/employee/viewbyall", method = RequestMethod.GET)
	public org.json.simple.JSONObject viewByAllEmployee(@RequestParam(value = "offset", defaultValue = "0") int offset,
			@RequestParam(value = "limit", defaultValue = "10") int limit,
			@RequestParam(value = "user_privilege", defaultValue = "3") String user_privilege,
			@RequestParam(value = "emailid") String emailid) {

//		if(user_privilege.equals("2"))
//		{
//			List<Employee> Employeecurrentstatus= employeerepository.findByCurrentstatus("bench");
//			org.json.simple.JSONObject js=new org.json.simple.JSONObject();
//			js.put( "employeeList",Employeecurrentstatus);
//			return js;
//			
//		}

		// return employeeRepository.findAll();
		return employeeSearchRepository.getAllEmployees(offset, limit, user_privilege, emailid);

	}

}
