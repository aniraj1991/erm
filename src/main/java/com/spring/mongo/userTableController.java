/*******************************************************************************
 * Copyright ©2002-2014 Skava.
 * All rights reserved.The Skava system, including
 * without limitation, all software and other elements
 * thereof, are owned or controlled exclusively by
 * Skava and protected by copyright, patent, and
 * other laws. Use without permission is prohibited.
 *
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.spring.mongo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class userTableController {
	
	@Autowired
	public usersRepository users;

	@GetMapping("/Users/GetAll")
	public List<users> GetallData() {
		System.out.println("in get all");
		return (List<users>) users.findAll();
	}

	@GetMapping("/Users/GetById/{id}")
	public Optional<users> GetUserById(@PathVariable long id) {
		return users.findById(id);
	}

	@GetMapping("/Users/GetByEmail/{email}")
	public Optional<users> GetUserByEmail(@PathVariable String email) {
		return users.findByUseremail(email);
	}

	@GetMapping("/Users/GetByStatus/{status}")
	public Optional<users> GetUserByStatus(@PathVariable int status) {
		return users.findByUserstatus(status);
	}

	@GetMapping("/Users/GetByEmpId/{empId}")
	public Optional<users> GetUserByStatus(@PathVariable String empId) {
		return users.findByUserempid(empId);
	}

	@GetMapping("/Users/GetLogin/{usrId}/{usrPass}")
	public Optional<users> GetUserByStatus(@PathVariable String usrId, @PathVariable String usrPass) {
		return users.findByUseremailAndUserpwd(usrId, usrPass);
	}

	@PostMapping(value = "/Users/add", consumes = { "application/json" })
	public users createUser(@RequestBody users u) {
		System.out.println("in add user");
		System.out.println(u);
		users.save(u);
		return u;
	}
	
	@GetMapping("/users/update")
	public Optional<users> updatePassword(@RequestParam("emailid") String emailID, @RequestParam("password") String password) {
		
		//Optional<users> updatedUsr = users.updatePasswordByUseremail(password, emailID);
		//users.saveAll(updatedUsr);
		
		Optional<users> nu = users.findByUseremail(emailID);
		nu.get().setUserpwd(password);
		nu.get().setUserstatus(1);
		try {
			users.save(nu.get());
		}
		catch(Exception e) {
			System.out.print(e);
		}		
		return nu;
	}

	@GetMapping("/user/login")
	public ModelAndView getAllEmployee() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("userlogin");
		modelAndView.addObject("pagename","login");
		modelAndView.addObject("loginMsg","INIT");
		return modelAndView;
	}
	
	@GetMapping("/user/logout")
	public ModelAndView getLoggdOut() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("userlogout");
		modelAndView.addObject("pagename","logout");
		return modelAndView;
	}
	
	

	@PostMapping("/dologin")
	public ModelAndView doUserLogin(@ModelAttribute usercredentials userData, HttpSession session,
			HttpServletResponse cookieRes) {

		String GET_URL = "http://localhost:8102/Users/GetLogin/" + userData.getUsername() + "/"
				+ userData.getPassword();

		RestTemplate restTemplate = new RestTemplate();
		users result = restTemplate.getForObject(GET_URL, users.class);

		ModelAndView modelAndView = new ModelAndView();

		if (result != null && result.getId() > 0) {
			
			GET_URL = "http://localhost:8102/Users/Role/GetById/" + result.getId();
			userRoles rolesData = restTemplate.getForObject(GET_URL, userRoles.class);

			Map<String, String> sessionData = new HashMap<String, String>();
			sessionData.put("empId", result.getUserempid());
			sessionData.put("email", result.getUseremail());
			sessionData.put("name", result.getUsername());
			sessionData.put("status", Integer.toString(result.getUserstatus()));
			sessionData.put("privilegeType", Integer.toString(rolesData.getPrivilegeid()));

			session.setAttribute("user-data", sessionData);
			cookieRes.addCookie(new Cookie("user-privilege", Integer.toString(rolesData.getPrivilegeid())));
			cookieRes.addCookie(new Cookie("isLoggedIn", "true"));
			cookieRes.addCookie(new Cookie("user-id", result.getUseremail()));
			
			if(result.getUserstatus() == 2) {
				modelAndView.setViewName("updatepass");
				modelAndView.addObject("emailid", result.getUseremail());
			}
			else {
				modelAndView.setViewName("view");
				return new ModelAndView("redirect:"+"http://localhost:8102/employee/projectview");
			}

			
		} else {
			modelAndView.setViewName("userlogin");
			modelAndView.addObject("loginMsg","ERROR");
		}
		
		return modelAndView;
	}
	
	@PostMapping("/viewmenu")
	public ModelAndView changeUserView(@ModelAttribute usercredentials usrData) {

		String URL = "http://localhost:8102/users/update?emailid=" + usrData.getUsername() + "&password="
				+ usrData.getPassword();

		RestTemplate restTemplate = new RestTemplate();
		//users result = restTemplate.getForObject(URL, users.class);
		users result = restTemplate.getForObject(URL, users.class);

		ModelAndView modelAndView = new ModelAndView();

		if (result != null && result.getId() > 0) {
			
			modelAndView.setViewName("view");
			return new ModelAndView("redirect:"+"http://localhost:8102/employee/projectview");	
		}
		else {
			modelAndView.setViewName("userlogin");
			modelAndView.addObject("loginMsg","ERROR");
		}
		
		return modelAndView;
	}
}

/*
 * String GET_URL = "http://localhost:8091/Users/GetLogin/" +
 * userData.getUsername() + "/" + userData.getPassword();
 * 
 * POST:
 * 
 * RestTemplate restTemplate = new RestTemplate(); Map<String, String> params =
 * new HashMap<String, String>(); params.put("prop1", "1"); params.put("prop2",
 * "value"); String result = restTemplate.getForObject(GET_URL, String.class,
 * params);
 * 
 * GET:
 * 
 * RestTemplate restTemplate = new RestTemplate(); String result =
 * restTemplate.getForObject(GET_URL, String.class);
 */
