/*******************************************************************************
 * Copyright ©2002-2014 Skava.
 * All rights reserved.The Skava system, including
 * without limitation, all software and other elements
 * thereof, are owned or controlled exclusively by
 * Skava and protected by copyright, patent, and
 * other laws. Use without permission is prohibited.
 *
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.spring.mongo;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface userRolesReposetory extends CrudRepository<userRoles, Long>{

	Optional<userRoles> findByUserid(int id);

	Optional<userRoles> findByPrivilegeid(int priId);
}
