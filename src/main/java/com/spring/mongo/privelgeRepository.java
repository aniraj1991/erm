/*******************************************************************************
 * Copyright ©2002-2014 Skava.
 * All rights reserved.The Skava system, including
 * without limitation, all software and other elements
 * thereof, are owned or controlled exclusively by
 * Skava and protected by copyright, patent, and
 * other laws. Use without permission is prohibited.
 *
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.spring.mongo;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@Repository
public interface privelgeRepository extends CrudRepository<privilege,String> {

	Optional<privilege> findByPrivilegetype(String type);

}
