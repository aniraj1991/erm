package com.spring.mongo;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface OrderRepository extends MongoRepository<project, String> {

	/*Optional<project> findByEmailId(String ST);

	// @Query(value = "{ 'project' : ?0, 'project.projectname' : ?1 }", fields = "{ 'project.projectname' : 'LLBean' }")
	@Query(value = "{ 'project.projectId' : ?1 }")
	List<Mydb> projectname(String project, String projectname);
	*/
	
	List<project> findByNameRegexOrDescriptionRegexOrClientRegex(String ST,String ST1,String ST2);
	
	List<project> findByName(String ST);
	
	/*
	 public Collection<project> getEmployees(String text) {
	        return mongoTemplate.find(Query.query(new Criteria()
	                        .orOperator(Criteria.where("name").regex(text, "i"))
	                        ), project.class);
	    }*/
	
	/* public  List<project> findByNameOrDescriptionorClient(String text) {
	        return OrderRepository.find(Query.query(new Criteria()
	                        .orOperator(Criteria.where("name").regex(text, "i"),
	                                    Criteria.where("description").regex(text, "i"),
	                                    Criteria.where("client").regex(text, "i"))
	                        ), project.class);
	    }*/
	    
	    
	
}
