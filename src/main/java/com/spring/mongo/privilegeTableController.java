/*******************************************************************************
 * Copyright ©2002-2014 Skava.
 * All rights reserved.The Skava system, including
 * without limitation, all software and other elements
 * thereof, are owned or controlled exclusively by
 * Skava and protected by copyright, patent, and
 * other laws. Use without permission is prohibited.
 *
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.spring.mongo;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.beans.factory.BeanInitializationException;


@RestController
public class privilegeTableController {
	
	@Autowired
	public privelgeRepository privilege;
	
	@GetMapping("/Users/GetPrivilege")
	public List<privilege> GetPrivilegeData()
	{
		return (List<privilege>) privilege.findAll();
	}
	
	@GetMapping("/Users/GetPrivilegeByType/{type}")
    public Optional<privilege> GetUser(@PathVariable String type)
    {
    	return privilege.findByPrivilegetype(type);
    }
	
	@PostMapping(value="/Users/AddPrivilege", consumes = {"application/json"})
	public privilege createCity(@RequestBody privilege p)
	{
		System.out.println(p);
		privilege.save(p);
		return p;
	}
}
