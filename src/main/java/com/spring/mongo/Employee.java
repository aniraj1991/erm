package com.spring.mongo;




import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

//import com.tests4geeks.tutorials.model.Project;

import java.sql.Date;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
/*import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;*/
import org.springframework.format.annotation.DateTimeFormat;
@Document
public class Employee {
	//@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
	@Getter @Setter private String id;	
	
	//@Size(min=2, max=32, message="First name must be between 2 and 32 characters")
	private String name;
	
	//@Size(min=2, max=32, message="Last name must be between 2 and 32 characters")
	private String lname;
	
	//@Indexed(unique = true)
	//@Pattern(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", message="Email address is invalid")
	private String emailId;
	
	//@Indexed(unique = true)
	//@Size(max = 20, min = 10)
	//@NotBlank(message = "Employee id is mandatory")
	//private String employeeId;
	
	////@Past(message="{Past.employee.doj}")
	//@DateTimeFormat(pattern="dd/MM/yyyy")
	
	//@NotBlank(message = "Date of joing is mandatory")
	private String doj;
	@Setter @Getter private  String currentstatus;
	
	//@Future(message="{Past.employee.dob}")
	//@DateTimeFormat(pattern = "dd/MM/yyyy")
	//@NotBlank(message = "Date of birth is mandatory")
	private String dob;
	
	//@NotBlank(message = "Designation is mandatory")
	private String designation;
	private String picture,pid,pname; 
	//@NotBlank(message = "Please enter the Details")
	private String address, contact, experience,skillSet;
	@Setter @Getter private String projectDetails;
	@Setter @Getter private String  client;
	@Setter @Getter private String   userRole;
	//@NotNull(message="Please enter a number of hours per week")
	//@Min(1)
	//@Max(10)
	private Integer level;
	private String managerDetails;
	private String[]  customProperties;
	public String[] getCustomProperties() {
		return customProperties;
	}
	public void setCustomProperties(String[] customProperties) {
		this.customProperties = customProperties;
	}
	
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getDoj() {
		return doj;
	}
	public void setDoj(String doj) {
		this.doj = doj;
	}
	/*public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}*/
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSkillSet() {
		return skillSet;
	}
	public void setSkillSet(String skillSet) {
		this.skillSet = skillSet;
	}
	public String getManagerDetails() {
		return managerDetails;
	}
	public void setManagerDetails(String managerDetails) {
		this.managerDetails = managerDetails;
	}
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	
	public String getStartDate() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
