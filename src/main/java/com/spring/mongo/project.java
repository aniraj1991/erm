package com.spring.mongo;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document
public class project {
	@Getter @Setter private String name;
	@Getter @Setter private String id;	
	@Getter @Setter private String client;
	@Getter @Setter private String region;
	@Getter @Setter private String startDate;
	@Getter @Setter private String endDate;
	
	
	@Getter @Setter private String stackUsed;
	@Getter @Setter private String description;
	@Getter @Setter private String managername;
	//@Getter @Setter private List<employee> employeename;
	@Getter @Setter private String typeofproject;
	
}
