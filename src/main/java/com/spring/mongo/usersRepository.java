/*******************************************************************************
 * Copyright ©2002-2014 Skava.
 * All rights reserved.The Skava system, including
 * without limitation, all software and other elements
 * thereof, are owned or controlled exclusively by
 * Skava and protected by copyright, patent, and
 * other laws. Use without permission is prohibited.
 *
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.spring.mongo;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface usersRepository extends CrudRepository<users, Long>{

	Optional<users> findByUseremail(String email);

	Optional<users> findByUserstatus(int status);

	Optional<users> findByUserempid(String empId);

	Optional<users> findByUseremailAndUserpwd(String usrId, String usrPass);

	int findIdByUseremail(String username);
	
	/*@Query("insert into users(userpwd) values(?1) where users.useremail = ?2")
	Optional<users> updatePasswordByUseremail(String password, String email);*/

	users save(Optional<users> usr);

}
