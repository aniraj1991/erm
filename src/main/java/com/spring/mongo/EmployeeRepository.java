package com.spring.mongo;

import java.util.List;
import java.util.Optional;

//import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.repository.MongoRepository;
//import org.springframework.data.mongodb.repository.MongoRepository;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//import com.Service.EmployeeModel.Employee;

@Repository
public interface EmployeeRepository extends MongoRepository<Employee, String>{
	
	Optional<Employee> findById(String ST);
	
	List<Employee> findByCurrentstatus(String ST);
	Employee findByEmailId(String ST);

	Employee findByNameRegexOrIdRegexOrEmailIdRegex(String ST,String ST1,String ST2);
	
}
