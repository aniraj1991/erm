package com.spring.mongo;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document
public class managerDetails {
	@Getter @Setter private String managerName;
	@Getter @Setter private String managerId;
	@Getter @Setter private String project;
	@Getter @Setter private String isActive;
}
