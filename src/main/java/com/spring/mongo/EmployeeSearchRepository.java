package com.spring.mongo;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;
import com.spring.mongo.Employee;
import com.spring.mongo.EmployeeRepository;
import org.springframework.web.util.WebUtils;

@Repository
public class EmployeeSearchRepository {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	EmployeeRepository employeeRepository;
	
	EmployeeSearchRepository()
	{
		
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getAllEmployees(int offset,int limit, String user_privilege,String emailid) {
		Query query = new Query();
        query.skip(offset);
        query.limit(limit);
       
        JSONObject obj = new JSONObject();
        if(user_privilege.equals("2"))
        {
        	//WebUtils.getCookie(http://localhost:8102/employee/employeeview, "user-id");

        	query.addCriteria(new Criteria().orOperator(Criteria.where("currentstatus").regex("bench", "i"),Criteria.where("emailId").regex(emailid, "i")));
        }
        
        if(user_privilege.equals("3"))
        {
        	//WebUtils.getCookie(http://localhost:8102/employee/employeeview, "user-id");

        	query.addCriteria(new Criteria().where("emailId").regex(emailid, "i"));
        }
        
        obj.put("employeeList", mongoTemplate.find(query, Employee.class));
        return obj;
        
		
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getAllEmployeesbycurrent(int offset,int limit) {
		Query query = new Query();
        query.skip(offset);
        query.limit(limit);
       
        JSONObject obj = new JSONObject();
       
        obj.put("employeeList", mongoTemplate.find(query, Employee.class));
        return obj;
        
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<project> getAllProject(int offset,int limit) {
		Query query = new Query();
        query.skip(offset);
        query.limit(limit);
       
        JSONObject obj = new JSONObject();
       
        //obj.put("productlist", );
        
        return mongoTemplate.find(query, project.class);
        
		
	}
	
	public Collection<Employee> getEmployees(String text) {
		return mongoTemplate.find(Query.query(new Criteria()
						.orOperator(Criteria.where("name").regex(text, "i"), 
									Criteria.where("employeeId").regex(text, "i"), 
									Criteria.where("emailId").regex(text, "i"))
						), Employee.class);
	}
	public Optional<Employee> getEmployeesById(String text) {
		//return mongoTemplate.find(Query.query(new Criteria().where("employeeId").regex(text, "i")), Employee.class);
		Optional<Employee> result = employeeRepository.findById(text);
		//System.out.println(result.id);
		 return result;
	}
	public Collection<Employee> getEmployeesByEmailId(String text) {
		//new Criteria();
		return mongoTemplate.find(Query.query(Criteria.where("emailId").regex(text, "i")), Employee.class);
	}
	public Collection<Employee> getEmployeesByName(String text) {
		//new Criteria();
		return mongoTemplate.find(Query.query(Criteria.where("name").regex(text, "i")), Employee.class);
	}
	
	public Collection<Employee> getManagerDetails(String text) {
		int level = 4;
		return mongoTemplate.find(Query.query(new Criteria().andOperator(new Criteria().orOperator(Criteria.where("name").regex(text, "i"), 
				Criteria.where("employeeId").regex(text, "i"), Criteria.where("emailId").regex(text, "i")),Criteria.where("level").gte(level))), Employee.class);
	}
}
